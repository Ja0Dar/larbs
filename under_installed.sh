#!/bin/bash

function under {
	for prog in $(cat ./progs.csv | grep -v -E "^\#" | grep -v -E "^G" | tail -n +2 | cut -d, -f2); do
		echo $(pacman -Q | awk '{print $1}' | grep -E "^$prog$" || echo $prog NOT FOUND) | grep "NOT FOUND"
	done
}

function grepArg {
	echo -n "("
	under | cut -d " " -f1 | tr '\n' '|'
	echo -n ")"
}

cat ./progs.csv | grep -E ",$(grepArg),"
