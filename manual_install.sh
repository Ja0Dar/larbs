#!/bin/bash
[ -d ~/.oh-my-zsh ] || sh -c "$(wget -O- https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

[ -d ~/.oh-my-zsh/custom/themes/powerlevel9k ] || git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k

[ -d ~/.config/omf ] || curl -L https://get.oh-my.fish | fish

function doom() {
	git clone https://github.com/hlissner/doom-emacs ~/.emacs.d_doom
	~/.emacs.d_doom/bin/doom quickstart
	ln -sf ~/.emacs.d_doom ~/.emacs.d
}

[ -d ~/.emacs.d_doom ] || doom

[ -d ~/git/foss/emacs/spotify.el ] || git clone https://github.com/danielfm/spotify.el.git ~/git/foss/emacs/spotify.el
[ -d ~/git/foss/emacs/emacs-fish-completion ] || git clone https://gitlab.com/JakDar/emacs-fish-completion ~/git/foss/emacs/emacs-fish-completion

[ -d ~/git/foss/intelli-space ] || git clone https://github.com/MarcoIeni/intelli-space ~/git/foss/intelli-space
[ -L ~/intelli-space ] || ln -s ~/git/foss/intelli-space ~/intelli-space

[ -f /usr/local/bin/metals-emacs ] || sudo coursier bootstrap \
	--java-opt -Xss4m \
	--java-opt -Xms100m \
	--java-opt -Dmetals.client=emacs \
	org.scalameta:metals_2.12:0.7.5 \
	-r bintray:scalacenter/releases \
	-r sonatype:snapshots \
	-o /usr/local/bin/metals-emacs -f

#https://scalameta.org/scalafmt/docs/installation.html#coursier
[ -f /usr/local/bin/scalafmt ] || sudo coursier bootstrap org.scalameta:scalafmt-cli_2.12:2.0.1 -r sonatype:snapshots -o /usr/local/bin/scalafmt --standalone --main org.scalafmt.cli.Cli

scalafmt --version # should be 2.0.1

function reason-lsp() {
	wget https://github.com/jaredly/reason-language-server/releases/download/1.7.12/rls-linux.zip
	unzip rls-linux.zip
	mkdir -p ~/.local/bin/lsp
	mv rls-linux/reason-language-server ~/.local/bin/lsp/reason-language-server
	rm -rf rls-linux.zip reason-language-server
}

[ -f ~/bin/lsp/reason-language-server ] || reason-lsp

pip3 install pyfunctional pandas numpy matplotlib flask --user
pip3 install virtualenv rope yapf flake8 black --user

[ -d ~/.local/share/nvim/site/autoload/plug.vim ] || curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

[ -d ~/git/foss/config/i3-quickterm ] || mkdir -p ~/git/foss/config/i3-quickterm &&
	git clone https://github.com/lbonn/i3-quickterm.git ~/git/foss/config/i3-quickterm &&
	mkdir -p ~/.local/bin/i3cmds &&
	ln -s ~/git/foss/config/i3-quickterm/i3-quickterm ~/.local/bin/i3cmds/i3-quickterm

function almond-install() {
	SCALA_VERSION=2.12.8
	ALMOND_VERSION=0.6.0

	coursier bootstrap \
		-r jitpack \
		-i user -I user:sh.almond:scala-kernel-api_$SCALA_VERSION:$ALMOND_VERSION \
		sh.almond:scala-kernel_$SCALA_VERSION:$ALMOND_VERSION \
		-o almond
	./almond --install
	rm almond
}

jupyter kernelspec list | grep scala || almond-install
