#!/bin/bash

default=$(
	pacman -Sg base base-devel | awk '{print $2}'
	cat progs.csv | cut -d, -f2
	cat optional.csv | cut -d, -f2
	echo yay grub
)

for prog in $(pacman -Qe | cut -d " " -f1); do
	echo $(echo $default | grep $prog || echo $prog NOT SYNCED) | grep "NOT SYNCED" # might be substring issue
done
